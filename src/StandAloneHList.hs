{-# LANGUAGE ExistentialQuantification,
             StandaloneDeriving,
             UndecidableInstances,
             KindSignatures,
             DataKinds,
             TypeOperators,
             GADTs,
             TypeFamilies,
             FlexibleInstances,
             MultiParamTypeClasses,
             PolyKinds,
             FunctionalDependencies,
             ConstraintKinds,
             DeriveDataTypeable,
             EmptyDataDecls,
             FlexibleContexts,
             GeneralizedNewtypeDeriving,
             RankNTypes,
             ScopedTypeVariables,
             StandaloneDeriving,
             OverlappingInstances#-}


module StandAloneHList where

-- | Definition of the Type for Heterogeneous Lists

--data HList (l::[*]) where
--  HNil  :: HList '[]
--  HCons :: e -> HList l -> HList (e ': l)

data family HList (l::[*])

data instance HList '[] = HNil
data instance HList (x ': xs) = x `HCons` HList xs

instance Show (HList '[]) where
      show _ = "H[]"

instance (Show e, Show (HList l)) => Show (HList (e ': l)) where
    show (HCons x l) = let 'H':'[':s = show l
                       in "H[" ++ show x ++
                                  (if s == "]" then s else "," ++ s)


class HExtend e l where
  type HExtendR e l
  (.*.) :: e -> l -> HExtendR e l

infixr 2 .*.
infixr 2 `HCons`
 
instance HExtend e (HList l) where
  type HExtendR e (HList l) = HList (e ': l)
  (.*.) = HCons


ls  = (3 ::Int) `HCons` True `HCons` "bla" `HCons` HNil
lst = (3 ::Int) .*. True .*. "bla" .*. HNil
  

-- | Definition of head
hHead :: HList (e ': l) -> e
hHead (HCons e l) = e

-- | Definition of tail
hTail :: HList (e ': l) -> HList l
hTail (HCons e l) = l

-- | Definition of append
-- type level append
type family AppendT (l::[k]) (m::[k]) :: [k]
type instance AppendT '[] m      = m
type instance AppendT (e ': l) m = e ': AppendT l m

-- term level definition
class AppendList l1 l2 where
  hAppend :: HList l1 -> HList l2 -> HList (AppendT l1 l2)

instance AppendList '[] l2 where
  hAppend HNil l2 = l2

instance (AppendList l1 l2) => AppendList (e ': l1) l2 where
  hAppend (HCons e l1) l2 = e `HCons` hAppend l1 l2

-- | Definition of reverse
-- type level reverse
type family ReverseT (l::[k]) :: [k]
type instance ReverseT '[]      = '[]
type instance ReverseT (e ': l) = AppendT (ReverseT l) '[e]

--class ReverseList l where
--  hReverse :: HList l -> HList (ReverseT l)

--term level reverse
class RevAppend l1 l2 l3 | l1 l2 -> l3 where
  hRevAppend :: HList l1 -> HList l2 -> HList l3

instance RevAppend '[] l2 l2 where
  hRevAppend _ l2 = l2

instance RevAppend l1 (e ': l2) l3 => RevAppend (e ': l1) l2 l3 where
  hRevAppend (HCons e l1) l2 = hRevAppend l1 (HCons e l2)


hReverse_ l = hRevAppend l HNil

-- | definition of last
hLast :: RevAppend l '[] (e ': l2) => HList l -> e
hLast l = hHead $ hReverse_ l 

-- | definition of lxsength
-- type level length

type family Length (l::[*]) :: Nat
type instance Length '[]      = Z
type instance Length (e ': l) = S (Length l)

hLength :: HList (l::[*]) -> Proxy (Length l)
hLength _ = Proxy

hLength_ :: ReifyNat (Length l) => HList l -> Nat
hLength_ l = reifyNat $ hLength l


-- | Update at nat

class UpdateAtNat (n::Nat) v (l::[*]) where
  type UpdateAtNatR n v l:: [*]
  hUpdateAtNat :: Proxy n -> v -> HList l -> HList (UpdateAtNatR n v l)

instance UpdateAtNat Z v (e ': l) where
  type UpdateAtNatR Z v (e ': l) = (v ': l)
  hUpdateAtNat Proxy v (HCons e l) = HCons v l

instance (UpdateAtNat n v l) => UpdateAtNat (S n) v (e ': l) where
  type UpdateAtNatR (S n) v (e ': l) = (e ': (UpdateAtNatR n v l))
  hUpdateAtNat Proxy v (HCons e l) = HCons e (hUpdateAtNat (Proxy::Proxy n) v l)

-- esto tambien anda y me parece mejor 

class SUpdateAtNat (n::Nat) v (l::[*]) where
  type SUpdateAtNatR n v l:: [*]
  hSUpdateAtNat :: Natty n -> v -> HList l -> HList (SUpdateAtNatR n v l)

instance SUpdateAtNat Z v (e ': l) where
  type SUpdateAtNatR Z v (e ': l) = (v ': l)
  hSUpdateAtNat Zy v (HCons e l) = HCons v l

instance (SUpdateAtNat n v l) => SUpdateAtNat (S n) v (e ': l) where
  type SUpdateAtNatR (S n) v (e ': l) = (e ': (SUpdateAtNatR n v l))
  hSUpdateAtNat (Sy ny) v (HCons e l) = HCons e (hSUpdateAtNat ny v l)

-- example : hUpdateAtNat (Proxy :: Proxy 'Z) False lst = H[False,True,"bla"]

-------------------------------------------------------------------------------
-- Records
-------------------------------------------------------------------------------
-- tagged
newtype Tagged s b = Tagged { unTagged :: b } deriving (Show, Eq)

labelLVPair :: Tagged l v -> Label l
labelLVPair _ = Label

newLVPair :: Label l -> v -> Tagged l v
newLVPair _ = Tagged

infixr 4 .=.
{- Create a value with the given label. Analagous to a data
  constructor such as 'Just', 'Left', or 'Right'. Higher fixity
  than record-modification operations like ('.*.'), ('.-.'), etc. to
  support expression like the below w/o parentheses:

  >>> x .=. "v1" .*. y .=. '2' .*. emptyRecord
  Record{x="v1",y='2'}
-}

(.=.) :: Label l -> v -> Tagged l v
l .=. v = newLVPair l v

newtype Record (r :: [*]) = Record (HList r)

data Label l = Label

labelToProxy :: Label l -> Proxy l
labelToProxy _ = Proxy

class ShowLabel l where
  showLabel :: Label l -> String

mkRecord :: {-HRLabelSet r =>-} HList r -> Record r
mkRecord = Record

emptyRecord :: Record '[]
emptyRecord = mkRecord HNil


instance {-HRLabelSet (t ': r)
    => -} HExtend t (Record r) where
  type HExtendR t (Record r) = Record (t ': r)
  f .*. (Record r) = mkRecord (HCons f r)


class HRLabelSet (l::[*])
instance HRLabelSet '[]





-- | aplicar funciones

-- https://hackage.haskell.org/package/HList-0.4.1.0/
-- docs/src/Data-HList-FakePrelude.html#Apply

--Note that @class ApplyAB@ has three parameters and no functional dependencies.
-- Instances should be written in the style:

 -- instance (int ~ Int, double ~ Double) => ApplyAB Fn int double
 --  where applyAB _ = fromIntegral

-- rather than the more natural

-- instance ApplyAB Fn Int Double

-- The first instance allows types to be inferred as if we had
-- @class ApplyAB a b c | a -> b c@, while the second instance
-- only matches if ghc already knows that it needs
-- @ApplyAB Fn Int Double@. Since @applyAB Fn :: Int -> Double@
-- has a monomorphic type, this trimmed down example does not
-- really make sense because @applyAB (fromIntegral :: Int -> Double)@
-- is exactly the same. Nontheless, the other uses of @ApplyAB@
-- follow this pattern, and the benefits are seen when the type of
-- @applyAB Fn@ has at least one type variable.

-- Additional explanation can be found
-- in <http://okmij.org/ftp/Haskell/typecast.html#local-fd
--local functional dependencies>


class ApplyAB f a b where
  hApply :: f -> a -> b

class Fun f
instance Fun Dup

data Dup = Dup

instance (natA ~ Nat, natB ~ Nat) => ApplyAB Dup natA natB where
  hApply Dup n = duplicate n
    where duplicate Z     = Z
          duplicate (S n) = S $ S $ duplicate n


-- mapping

class Map f (l::[*]) (l'::[*]) where
  hMap :: f -> HList l -> HList l' 

instance (Fun f) => Map f '[] '[] where
  hMap _ HNil = HNil


data Natty (n :: Nat) where
  Zy :: Natty Z
  Sy :: Natty n -> Natty (S n)



-- | map
--class Map f (l::[*]) (fl::[*]) where
--  hMap :: f -> HList l -> HList fl 

--instance Map f '[] '[] where
--  hMap _ HNil = HNil

--instance (Apply f e fe, Map f l fl) => Map f (e ': l) (fe ': fl) where
--  hMap f (HCons e l) = HCons (apply f e) $ hMap f l

--class Map f (l::[*]) where
--  type TMapfl :: [*]
--  hMap :: f -> HList l -> HList TMapfl 

--instance Map f '[] where
--  type TMapfl = '[]
--  hMap _ HNil = HNil

--instance (Apply f e fe, Map f l) => Map f (e ': l) where
--  type TMapfl = '[]
--  hMap f (HCons e l) = HCons (apply f e) $ hMap f l

-- arranco de nuevo...
-- Type level  map
{-
type family TMap (f:: k -> k) (l::[k]) :: [k]
type instance TMap f '[] = '[]
type instance TMap f (e ': l) = (f e) ': (TMap f l)



-- | catamorfismo
class Foldr f v (l::[*]) r where
  hFoldr :: f -> v -> HList l -> r

instance (v ~ v') => Foldr f v '[] v' where
  hFoldr f v HNil = v

class Apply f a r | f a -> r where
  apply :: f -> a -> r
 -}

-- | stuff para aplicar funciiones

--class TNat n
--instance TNat Z
--instance TNat n => TNat (S n)
{-
data WNat :: Nat -> * where
  WZ    :: WNat Z 
  WSucc :: WNat n -> WNat (S n)
deriving instance Show (WNat n)
class ApplyN f (a::Nat) (r::Nat) | f a -> r where
  applyN :: f -> WNat a -> WNat r

data Dup = Dup

instance ApplyN Dup Z Z where
  applyN Dup WZ = WZ

instance (ApplyN Dup n nby2) => ApplyN Dup (S n) (S (S nby2)) where
  applyN Dup (WSucc n) = WSucc $ WSucc $ applyN Dup n

type family Mby2 (n::Nat) :: Nat
type instance Mby2 Z     = Z
type instance Mby2 (S n) = S (S (Mby2 n))



-}



{-  
instance ReverseList '[] where
  hReverse HNil = HNil

instance ReverseList l => ReverseList (e ': l) where
  hReverse (HCons e l) = (hReverse l) `hAppend` (e `HCons` HNil)

> ____Could not deduce (AppendList (ReverseT * l) ((':) * e ('[] *)))
      arising from a use of `hAppend'
    

--hReverse :: HList l -> HList (ReverseT l)
--hReverse HNil = HNil
--hReverse (HCons e l) = hAppend (hReverse l) (HCons e l)                      


????
class Rev xs sx where
  rev :: HList xs -> HList sx 

instance Rev xs (ReverseT xs) where
  rev HNil = HNilx

-}

-- | Bools

data Boolean = HTrue | HFalse deriving Show

-- | Nats

data Nat = Z | S Nat

instance Show Nat where
  show Z     = "n0"
  show (S n) = let 'n':n_ = show n
               in  'n':(show ( 1+(read n_ :: Int)))

type family Sum (m::Nat)(n::Nat)::Nat
type instance Sum Z n     = n
type instance Sum (S m) n = S (Sum m n)

 
-- | Proxies

data Proxy (a::k) where
  Proxy :: Proxy a

-- | from proxy to nats

class ReifyNat (n::Nat) where
  reifyNat :: Proxy n -> Nat

instance ReifyNat Z where
  reifyNat _ = Z

instance ReifyNat n => ReifyNat (S n) where
  reifyNat _ = S $ reifyNat (Proxy :: Proxy n)

