{-# LANGUAGE GADTs,
             DataKinds,
             KindSignatures,
             StandaloneDeriving,
             RankNTypes,
             PolyKinds,
             TypeFamilies,
             TypeOperators #-}

-------------------------------------------------------------------------------
module Vector where

data Nat = Z | S Nat deriving Eq

data Proxy :: k -> * where
  Proxy :: Proxy k

data SNat :: Nat -> * where
  SZ :: SNat Z
  SS :: SNat n -> SNat (S n)

class Natty n where
  natty :: SNat n

instance Natty Z where
  natty = SZ

instance (Natty n) => Natty (S n) where
  natty = SS natty

data Vec :: * -> Nat -> * where
  VZ :: Vec a Z
  VS :: a -> Vec a n -> Vec a (S n)

(.:) a l = VS a l
infixr 2 .:

-------------------------------------------------------------------------------
-- head
-------------------------------------------------------------------------------
vHead :: Vec a (S n) -> a
vHead (VS a as) = a 


-------------------------------------------------------------------------------
-- Tail
-------------------------------------------------------------------------------
vTail :: Vec a (S n) -> Vec a n
vTail (VS a as) = as

-- hasta ahora las funciones son no recursivas, y la definicion es trivial

-------------------------------------------------------------------------------
--  Map
-------------------------------------------------------------------------------

vMap :: (a -> b) -> Vec a n -> Vec b n
vMap f VZ        = VZ
vMap f (VS a as) = VS (f a) $ vMap f as

-- Si bien tenemos una funcion recursiva,
-- No es necesario nada extranio, el compilador aprende, por ejemplo n ~ Z     

-------------------------------------------------------------------------------
--Cata
-------------------------------------------------------------------------------

vFoldr :: (a -> b -> b) -> b -> Vec a n -> b
vFoldr f v VZ        = v
vFoldr f v (VS a as) = f a (vFoldr f v as) 

-- overkill?:

--class VFoldr (n :: Nat) where
--  vFoldr :: (a -> b -> b) -> b -> Vec a n -> b

--instance VFoldr Z where
--  vFoldr f v VZ = v
-- ....  

-------------------------------------------------------------------------------
-- Appending
-------------------------------------------------------------------------------

-- vAppend :: Vec a n -> Vec a m -> Vec a k
-- algo asi no tipa 

type family (a::Nat) :+ (b::Nat) :: Nat
type instance Z     :+ n = n
type instance S k :+ n = S (k :+ n)

vAppend :: Vec a n -> Vec a m -> Vec a (n :+ m)
vAppend VZ v        = v
vAppend (VS a as) v = VS a $ vAppend as v

-------------------------------------------------------------------------------
-- Splitting
-------------------------------------------------------------------------------

-- vSplitAt :: Vec a (m :+ n) -> (Vec a m, Vec a n) 
-- algo asi no anda, se necesita m en tiempo de ejecucion

vSplitAt :: SNat m -> Vec a (m :+ n) -> (Vec a m, Vec a n) 
vSplitAt SZ v = (VZ, v)
vSplitAt (SS n) (VS a as) = let (v,v') = vSplitAt n as
                            in (VS a v, v')


-------------------------------------------------------------------------------
-- Taking
-------------------------------------------------------------------------------
-- vTake :: SNat m -> Vec a (m :+ n) -> Vec a m  --- no funciona
-- ghc no sabe como instanciar n, en realidad n esta bien determinado dada la
-- suma porque :+ es inyectiva, pero para el compilador es una funcion mas


--- resolvemos con proxys

vTake :: SNat m -> Proxy n -> Vec a (m :+ n) -> Vec a m  --- no funciona
vTake SZ n v             = VZ
vTake (SS m) n (VS a as) = VS a (vTake m n as)
-- > vTake (SS (SS SZ)) Proxy iv = v[1,3]



-------------------------------------------------------------------------------
-- Replicating
-------------------------------------------------------------------------------

vReplicate :: SNat n -> a -> Vec a n
vReplicate SZ a     = VZ
vReplicate (SS n) a = VS a (vReplicate n a)

--- >vReplicate (SS(SS(SS(SS SZ)))) 3 = v[3,3,3,3]



-------------------------------------------------------------------------------
-- Replicating 2 
-------------------------------------------------------------------------------
-- Pdemos replicar sin pasar parametro?

class VReplicate (n::Nat) where
  vReplicateC :: a -> Vec a n

instance VReplicate Z where
  vReplicateC _ = VZ

instance VReplicate n => VReplicate (S n) where
  vReplicateC a = VS a (vReplicateC a)
  
-- > vReplicateC 3 :: Vec Int (S (S Z)) = v[3,3]
--      (hay que anotar el tipo, a menos que por alguna razon se pueda inferir)





nv = 1 .: 3 .: (2::Nat) .: 4 .: VZ
iv = 1 .: 3 .: (2::Int) .: 4 .: VZ

instance (Show a,Eq a) => Show (Vec a n) where
  show (VZ)      = "v[]"
  show (VS x VZ) = "v["++ show x++"]"

  show (VS x xs) = let ('v':'[':lst) = show xs
                   in "v[" ++ show x ++ "," ++ lst
    
deriving instance Eq a => (Eq (Vec a n))

instance Num Nat where
  Z + n     = n
  (S m) + n = S (m + n)

  Z * n     = Z
  (S m) * n = n  + (m * n)

  abs = id

  signum Z = Z
  signum _ = S Z

  fromInteger = \n -> if n <= 0 then Z
                      else S (fromInteger (n - 1))                


instance Show Nat where
  show Z = "n0"
  show (S n) = let ('n':lst) = show n
               in 'n': show ((read lst) + 1)
